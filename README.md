# Calculator #

### Objective ###

This project is base upon the request of a test for https://www.mrandmrssmith.com/ company.
The test consists in a Symfony project for a calculator, which is expressed in the way of a HTML form.

### Project ###

The form consists of 2 text fields, 1 select dropdown with the available 
operations to perform, and a results show page.

The code is structured based in the built in capabilities of Symfony regarding form handling, which means creating the form as explained in the Forms section of Symfony documentation.

The PHP "front end" code consists of a DefaultController with 2 methods:

* "indexAction" that initially rendering the form
* "resultAction" which handles the rendering of the result

Despite this 2 operations being really simple, having 2 methods allows flexibility to perform both in a different way if needed, as is clear that the operation can be handled by 1 method only.

The "back end" portion of the code is 2 entities and 1 service as follows:

*Entities that defines a calculation operation, and the way a form would be rendered
*A service that performs the calculation itself (in this case, a simple switch case), but the interesting part is that it implements the "CalculatorInterface".

The reason for implementing that interface is for complying for (most of the) SOLID principles, specifically the "Open Closed" one. How? Well, the moment when the team decides that the logic behind the calculation is slow/obsolete/whatever, it can be changed only by implementing that interface, and the code will continue to work as expected, as the method that performs the core calculation is "performOperation". (In this specific case that could not be 100% true, but because of the way Symfony handles the creation of services)

### Installation ###

Just clone the repo, open a bash console (or equivalent) and execute *php bin/console server:run*, then open a browser window and execute "http://localhost:8000"


### Who do I talk to? ###

Any troubles in executing/installing the code, just email me at jmares79@gmail.com and I'll solve ASAP! :)