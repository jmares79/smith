<?php

namespace CalculatorBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Calculation
{
    /**
     * @Assert\NotBlank()
     */
    protected $firstValue;

    /**
     * @Assert\NotBlank()
     */
    protected $secondValue;

    protected $operation;

    protected $result;

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    public function getFirstValue()
    {
        return $this->firstValue;
    }

    public function setFirstValue($firstValue)
    {
        $this->firstValue = $firstValue;
    }

    public function getSecondValue()
    {
        return $this->secondValue;
    }

    public function setSecondValue($secondValue)
    {
        $this->secondValue = $secondValue;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }
}
