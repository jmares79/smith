<?php
namespace CalculatorBundle\Service;

use CalculatorBundle\Interfaces\CalculatorInterface;

class CalculatorService implements CalculatorInterface
{
    public function performOperation($calculation)
    {
        $firstValue = $calculation->getFirstValue();
        $secondValue = $calculation->getSecondValue();

        switch ($calculation->getOperation()) {
            case '+':
                return $firstValue + $secondValue;
                break;

            case '-':
                return $firstValue - $secondValue;
                break;

            case '/':
                return $firstValue / $secondValue;
                break;

            case '*':
                return $firstValue * $secondValue;
                break;

            case '&':
                return $firstValue & $secondValue;
                break;

            case '|':
                return $firstValue | $secondValue;
                break;
        }
    }
}
