<?php

namespace CalculatorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CalculationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstValue', TextType::class, array('label' => false, 'attr' => array('placeholder' => 'First value')))
            ->add('operation', ChoiceType::class, array(
                    'choices'  => array(
                        'Add' => '+',
                        'Substract' => '-',
                        'Multiply' => '*',
                        'Divide' => '/',
                        'And' => '&',
                        'Or' => '|'
                    ))
                )
            ->add('secondValue', TextType::class, array('label' => false, 'attr' => array('placeholder' => 'Second value')))
            ->add('calculate', SubmitType::class, array('label' => 'Calculate'));
    }
}
