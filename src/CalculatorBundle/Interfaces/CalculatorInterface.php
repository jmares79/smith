<?php

namespace CalculatorBundle\Interfaces;

interface CalculatorInterface
{
    public function performOperation($calculation);
}
