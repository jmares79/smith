<?php

namespace CalculatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use CalculatorBundle\Entity\Calculation;
use CalculatorBundle\Form\CalculationType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $calculation = new Calculation();

        $form = $this->createForm(CalculationType::class, $calculation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calculatorService = $this->container->get('app.calculator_service');
            $calculation = $form->getData();

            $result = $calculatorService->performOperation($calculation);;

            return $this->redirectToRoute('calculation_result', array('result' => $result));
        }

        return $this->render('CalculatorBundle:Default:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function resultAction($result)
    {
        return $this->render('CalculatorBundle:Default:result.html.twig', array(
            'result' => $result
        ));
    }
}
